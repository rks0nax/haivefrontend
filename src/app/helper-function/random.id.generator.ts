export function randomIdGenerator() {
  return '_' + Math.random().toString(26).substr(2, 15);
}
