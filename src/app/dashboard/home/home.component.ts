import { Component, OnInit } from '@angular/core';
import {InstitutionVerify} from '../../services/general';
import {SegmentService} from '../../services/segmentServices';

const MENU_COMPACT = [
  {
    icon: 'widgets',
    title: 'DASHBOARD',
    link: '/v1/dashboard/home',
    highlight: '/v1/dashboard/home'
  },
  {
    icon: 'book',
    title: 'ACADEMICS',
    link: '/v1/dashboard/academics/timetable',
    highlight: '/v1/dashboard/academics'
  },
  {
    icon: 'local_library',
    title: 'STUDENTS',
    link: '/v1/dashboard/students',
    highlight: '/v1/dashboard/students'
  },
  {
    icon: 'dvr',
    title: 'OPERATIONS',
    link: '/v1/dashboard/operations',
    highlight: '/v1/dashboard/operations'
  },
  {
    icon: 'group',
    title: 'STAFF',
    link: '/v1/dashboard/staff',
    highlight: '/v1/dashboard/staff'
  },
  {
    icon: 'data_usage',
    title: 'FINANCE',
    link: '/v1/dashboard/finance',
    highlight: '/v1/dashboard/finance'
  },
  {
    icon: 'settings',
    title: 'SETUP',
    link: '/v1/dashboard/setup/details',
    highlight: '/v1/dashboard/setup',
  },

];


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  institutionName: String;
  segmentData;
  selectedSegment: [number, number] = [0, 0];
  compactIcon = MENU_COMPACT;
  constructor(private institutionVerify: InstitutionVerify, private segmentService: SegmentService) { }

  ngOnInit() {
    this.institutionName = this.institutionVerify.data.getValue() ? this.institutionVerify.data.getValue()['name'] : '';
    this.segmentService.segmentData.asObservable().subscribe((segmentData) => {
      this.segmentData = segmentData;
    });
    this.selectedSegment = [this.segmentService.segmentSelected.board, this.segmentService.segmentSelected.stream];
  }

  segmentChange(boardChange) {
    if (boardChange) {
      this.selectedSegment[1] = 0;
    }
    this.segmentService.updateSelectedSegment(this.selectedSegment[0], this.selectedSegment[1]);
  }

  isLinkSelected(url) {
    return location.pathname.startsWith(url);
  }
}
