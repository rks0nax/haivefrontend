import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  tabs = [
    {
      title: 'Details Capture',
      route: '/v1/dashboard/setup/users/details',
    },
    {
      title: 'Add super-admins',
      route: '/v1/dashboard/setup/users/admins',
    },
  ];
  constructor(private router: Router) {
    if (location.pathname === '/v1/dashboard/setup/users') {
      router.navigateByUrl(this.tabs[0].route);
    }
  }

  ngOnInit() {
  }

}
