import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSuperAdminsComponent } from './add-super-admins.component';

describe('AddSuperAdminsComponent', () => {
  let component: AddSuperAdminsComponent;
  let fixture: ComponentFixture<AddSuperAdminsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSuperAdminsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSuperAdminsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
