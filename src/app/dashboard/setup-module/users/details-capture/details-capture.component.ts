import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-details-capture',
  templateUrl: './details-capture.component.html',
  styleUrls: ['./details-capture.component.scss']
})
export class DetailsCaptureComponent implements OnInit {

  openStudentDrawer = false;
  openTeacherDrawer = false;
  constructor() { }

  ngOnInit() {
  }

}
