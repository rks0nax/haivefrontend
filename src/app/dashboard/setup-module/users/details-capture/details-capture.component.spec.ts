import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsCaptureComponent } from './details-capture.component';

describe('DetailsCaptureComponent', () => {
  let component: DetailsCaptureComponent;
  let fixture: ComponentFixture<DetailsCaptureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsCaptureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsCaptureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
