import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  menuItems = [
    {
      title: 'Institution details',
      link: '/v1/dashboard/setup/details',
      highlight: '/v1/dashboard/setup/details'
    },
    {
      title: 'Organisation structure',
      link: '/v1/dashboard/setup/structure/classes',
      highlight: '/v1/dashboard/setup/structure/'
    },
    {
      title: 'Users',
      link: '/v1/dashboard/setup/users',
      highlight: '/v1/dashboard/setup/users'
    },
    {
      title: 'Templates',
      link: '/v1/dashboard/templates',
      highlight: '/v1/dashboard/templates'
    }
  ];
  constructor() {

  }

  ngOnInit() {
  }

}
