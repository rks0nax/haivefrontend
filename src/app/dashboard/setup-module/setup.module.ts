import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './home/home.component';
import {
  NbLayoutModule, NbRouteTabsetModule, NbSidebarModule, NbSidebarService, NbTabsetModule, NbThemeModule,
  NbThemeService
} from '@nebular/theme';
import {
  MatDatepickerModule, MatIconModule, MatNativeDateModule, MatRadioModule, MatSelectModule, MatSidenavModule,
  MatSnackBarModule
} from '@angular/material';
import {MenuModule} from '../../custom-modules/menu-module/menu.module';
import { InstitutionDetailsComponent } from './institution-details/institution-details.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SegmentsComponent} from './institution-details/segments/segments';
import { OrganisationStructureComponent } from './organisation-structure/organisation-structure.component';
import { ClassesComponent } from './organisation-structure/classes/classes.component';
import { HousesComponent } from './organisation-structure/houses/houses.component';
import { DepartmentsComponent } from './organisation-structure/departments/departments.component';
import { DesignationComponent } from './organisation-structure/designation/designation.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UsersComponent } from './users/users.component';
import { DetailsCaptureComponent } from './users/details-capture/details-capture.component';
import { AddSuperAdminsComponent } from './users/add-super-admins/add-super-admins.component';
import {DrawerModule} from '../../custom-modules/drawer-module/drawer.module';
import { StudentAddComponent } from './users/details-capture/student-add/student-add.component';
import { StaffAddComponent } from './users/details-capture/staff-add/staff-add.component';

const DASHBOARD_ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'details',
        component: InstitutionDetailsComponent
      },
      {
        path: 'structure',
        component: OrganisationStructureComponent,
        children: [
          {
            path: 'classes',
            component: ClassesComponent
          },
          {
            path: 'houses',
            component: HousesComponent
          },
          {
            path: 'departments',
            component: DepartmentsComponent
          },
          {
            path: 'designation',
            component: DesignationComponent
          }
        ]
      },
      {
        path: 'users',
        component: UsersComponent,
        children: [
          {
            path: 'details',
            component: DetailsCaptureComponent
          },
          {
            path: 'admins',
            component: AddSuperAdminsComponent
          }]
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DASHBOARD_ROUTES),
    FormsModule,
    ReactiveFormsModule,
    // Material Modules
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatSelectModule,
    MatSidenavModule,
    // Nebular Modules
    NbSidebarModule,
    NbLayoutModule,
    NbThemeModule.forRoot({name: 'default'}),
    NbTabsetModule,
    NbRouteTabsetModule,
    // Other Imports

    // Bootstrap
    NgbModule,

    // Custom Modules
    MenuModule,
    DrawerModule
  ],
  declarations: [HomeComponent,
    InstitutionDetailsComponent,
    SegmentsComponent,
    OrganisationStructureComponent,
    ClassesComponent,
    HousesComponent,
    DepartmentsComponent,
    DesignationComponent, UsersComponent, DetailsCaptureComponent, AddSuperAdminsComponent, StudentAddComponent, StaffAddComponent],
  providers: [
    // Nebular Providers
    NbThemeService, NbSidebarService
    // Static Providers
  ],
  bootstrap: [HomeComponent]
})
export class SetupModule { }
