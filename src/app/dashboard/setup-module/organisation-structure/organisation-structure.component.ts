import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-organisation-structure',
  templateUrl: './organisation-structure.component.html',
  styleUrls: ['./organisation-structure.component.scss']
})
export class OrganisationStructureComponent implements OnInit {

  tabs = [
    {
      title: 'Classes',
      route: '/v1/dashboard/setup/structure/classes',
    },
    {
      title: 'House/Teams',
      route: '/v1/dashboard/setup/structure/houses',
    },
    {
      title: 'Departments',
      route: '/v1/dashboard/setup/structure/departments',
    },
    {
      title: 'Designation',
      route: '/v1/dashboard/setup/structure/designation'
    }
  ];
  constructor(private router: Router) {
    if (location.pathname === '/v1/dashboard/setup/structure') {
      router.navigateByUrl(this.tabs[0].route);
    }
  }

  ngOnInit() {
  }

}
