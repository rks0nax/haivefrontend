import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {APIRoutes} from '../../../../api.routes';
import {MatSnackBar} from '@angular/material';
import {SegmentService} from '../../../../services/segmentServices';
import {Observable} from '../../../../../../node_modules/rxjs';
import {randomIdGenerator} from '../../../../helper-function/random.id.generator';

@Component({
  selector: 'app-houses',
  templateUrl: './houses.component.html',
  styleUrls: ['./houses.component.scss']
})
export class HousesComponent implements OnInit {

  housesData = {
    segmentId: '1',
    houses: []
  };
  activeInputName = '';
  activeInputDescription = '';
  constructor(private segmentService: SegmentService,
              private http: HttpClient,
              private apiRoutes: APIRoutes,
              private matSnackbar: MatSnackBar) {
    this.segmentService.segmentId.asObservable().subscribe(value => {
      this.housesData.segmentId = value;
      // Reset data fields
      this.housesData.houses = [];

      // Reset add fields
      this.activeInputName = '';
      this.activeInputDescription = '';
      // Get the subject categories of a segment
      this.http.get(this.apiRoutes.getHouses(value)).subscribe((res) => {
        this.housesData = res['message'] || this.housesData;
      }, (err) => {
        // Other errors
        console.error('Something went wrong');
        console.error(err);
      });
    });
  }

  ngOnInit() {
  }

  addHouses() {
    if (!this.activeInputName) {
      this.matSnackbar.open('Enter the house name', '', {
        duration: 3000
      });
      return;
    }
    this.housesData.houses.push({
      'name': this.activeInputName,
      'id': randomIdGenerator(),
      'description': this.activeInputDescription
    });
    this.activeInputName = this.activeInputDescription = '';
  }

  deleteHouses(index) {
    this.housesData.houses.splice(index, 1);
  }

  updateHouses() {
    this.http.post(this.apiRoutes.postHouses(), this.housesData).subscribe((res) => {
      this.matSnackbar.open('Successfully Updated', 'OK', {
        duration: 3000
      });
    }, (err) => {
        this.matSnackbar.open(err.error.message, 'OK', {
          duration: 3000
      });
    });
  }
}
