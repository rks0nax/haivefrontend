import { Component, OnInit } from '@angular/core';
import {forkJoin} from 'rxjs';
import {SegmentService} from '../../../../services/segmentServices';
import {HttpClient} from '@angular/common/http';
import {APIRoutes} from '../../../../api.routes';
import {randomIdGenerator} from '../../../../helper-function/random.id.generator';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.scss']
})
export class ClassesComponent implements OnInit {

  activeInputName = '';
  activeInputProgramSection = [{
    name: '',
    program: ''
  }];
  programmes = {
    segmentId: '',
    programmes: []
  };
  classesData = {
    segmentId: '',
    classes: []
  };
  constructor(private segmentService: SegmentService,
              private http: HttpClient,
              private apiRoutes: APIRoutes,
              private matSnackbar: MatSnackBar) {
    this.segmentService.segmentId.asObservable().subscribe(value => {
      this.classesData.segmentId = this.programmes.segmentId = value;
      // Reset data fields
      this.classesData.classes = [];
      this.programmes.programmes = [];

      // Reset add fields
      this.activeInputProgramSection = [{
        name: '',
        program: ''
      }];
      this.activeInputName = '';
      // Get the subject categories of a segment
      const getProgrammes = this.http.get(this.apiRoutes.getProgrammes(value));
      const getClasses = this.http.get(this.apiRoutes.getClasses(value));
      forkJoin([
        getProgrammes,
        getClasses
      ]).subscribe(([programmes, classes]) => {
        this.classesData = classes['message'] || this.classesData;
        this.programmes = programmes['message'] || this.programmes;
      }, (err) => {
        // Other errors
        console.error('Something went wrong');
        console.error(err);
      });
    });
  }

  ngOnInit() {
  }

  findProgrammeById(id) {
    for (const program of this.programmes.programmes) {
      if (program.id === id) {
        return program.name;
      }
    }
    return '';
  }

  addSection() {
    const activeLength = this.activeInputProgramSection.length - 1;
    if (!this.activeInputProgramSection[activeLength].name || !this.activeInputProgramSection[activeLength].program) {
      return;
    }
    this.activeInputProgramSection.push({
      name: '',
      program: ''
    });
  }

  removeSection(index) {
    this.activeInputProgramSection.splice(index, 1);
  }

  setProgram(index, id) {
    this.activeInputProgramSection[index].program = id;
  }

  deleteSection(i, j) {
    this.classesData.classes[i]['section'].splice(j, 1);
  }

  addClass() {
    if (!this.activeInputName) {
      return;
    }
    const classData = {
      name: this.activeInputName,
      section: [],
      id: randomIdGenerator()
    };
    for (const section of this.activeInputProgramSection) {
      classData.section.push({
        name: section['name'],
        program: section['program']
      });
    }
    this.classesData.classes.push(classData);
    this.activeInputName = '';
    this.activeInputProgramSection = [{
      name: '',
      program: ''
    }];
    console.log(this.classesData);
  }

  updateClasses() {
    this.http.post(this.apiRoutes.postClasses(), this.classesData).subscribe((res) => {
      this.matSnackbar.open(res['message'], '', {
        duration: 5000
      });
    }, (err) => {
      console.log(err);
      this.matSnackbar.open(err.error['message'], '', {
        duration: 5000
      });
    });
  }
}
