import { Component, OnInit } from '@angular/core';
import {SegmentService} from '../../../../services/segmentServices';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import {APIRoutes} from '../../../../api.routes';
import {randomIdGenerator} from '../../../../helper-function/random.id.generator';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.scss']
})
export class DepartmentsComponent implements OnInit {

  departmentData = {
    segmentId: '',
    departments: [],
  };
  activeInputName = '';
  activeInputSubDepartments = [];
  activeInputSubDepartmentName = '';
  editActive = -1;
  editActiveExisting = -1;
  constructor(private segmentService: SegmentService,
              private http: HttpClient,
              private apiRoutes: APIRoutes,
              private matSnackbar: MatSnackBar) {
    this.segmentService.segmentId.asObservable().subscribe(value => {
      this.departmentData.segmentId = value;
      // Reset data fields
      this.departmentData.departments = [];

      // Reset add fields
      this.activeInputName = '';
      this.activeInputSubDepartments = [];
      // Get the subject categories of a segment
      this.http.get(this.apiRoutes.getDepartments(value)).subscribe((res) => {
        this.departmentData = res['message'] || this.departmentData;
      }, (err) => {
        // Other errors
        console.error('Something went wrong');
        console.error(err);
      });
    });
  }

  ngOnInit() {
  }

  addSubDepartmentActive() {
    if (this.activeInputSubDepartmentName) {
      this.activeInputSubDepartments.push({
        name: this.activeInputSubDepartmentName,
        id: randomIdGenerator()
      });
      this.activeInputSubDepartmentName = '';
    }
  }

  addSubDepartment() {
    if (!this.activeInputName) {
      this.matSnackbar.open('Please enter a department name', '', {
        duration: 3000
      });
      return;
    }
    if (!this.departmentData.departments) {
      this.departmentData.departments = [];
    }
    this.departmentData.departments.push({
      name: this.activeInputName,
      id: randomIdGenerator(),
      subdepartments: this.activeInputSubDepartments,
      designations: []
    });
    this.activeInputSubDepartmentName = '';
    this.activeInputName = '';
    this.activeInputSubDepartments = [];
  }

  addSubDepartmentExisting(index, value) {
    if (!value) {
      return;
    }
    this.departmentData.departments[index].subdepartments.push({
      name: value,
      id: randomIdGenerator()
    });
  }

  updateDepartment() {
    this.http.post(this.apiRoutes.postDepartments(), this.departmentData).subscribe(() => {
      this.matSnackbar.open('Successfully updated', '', {
        duration: 3000
      });
    }, (err) => {
      this.matSnackbar.open('Something went wrong', '', {
        duration: 3000
      });
    });
  }

}
