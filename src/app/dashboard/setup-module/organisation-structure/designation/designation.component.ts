import { Component, OnInit } from '@angular/core';
import {SegmentService} from '../../../../services/segmentServices';
import {HttpClient} from '@angular/common/http';
import {randomIdGenerator} from '../../../../helper-function/random.id.generator';
import {APIRoutes} from '../../../../api.routes';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-designation',
  templateUrl: './designation.component.html',
  styleUrls: ['./designation.component.scss']
})
export class DesignationComponent implements OnInit {

  departmentData = {
    segmentId: '',
    departments: [],
  };
  activeInputName = { name: null, index: null};
  activeInputDesignations = [];
  activeInputDesignationName = '';
  editActive = -1;
  editActiveExisting = -1;
  constructor(private segmentService: SegmentService,
              private http: HttpClient,
              private apiRoutes: APIRoutes,
              private matSnackbar: MatSnackBar) {
    this.segmentService.segmentId.asObservable().subscribe(value => {
      this.departmentData.segmentId = value;
      // Reset data fields
      this.departmentData.departments = [];

      // Reset add fields
      this.activeInputName = { name: null, index: null};
      this.activeInputDesignations = [];
      this.activeInputDesignationName = '';
      this.editActive = -1;
      this.editActiveExisting = -1;

      // Get the subject categories of a segment
      this.http.get(this.apiRoutes.getDepartments(value)).subscribe((res) => {
        this.departmentData = res['message'] || this.departmentData;
      }, (err) => {
        // Other errors
        console.error('Something went wrong');
        console.error(err);
      });
    });
  }

  ngOnInit() {
  }

  setActiveInputName(i) {
    this.activeInputName.index = i;
    this.activeInputName.name = this.departmentData.departments[i].name;
  }

  addDesignationsActive() {
    if (this.activeInputDesignationName) {
      this.activeInputDesignations.push({
        name: this.activeInputDesignationName,
        id: randomIdGenerator()
      });
      this.activeInputDesignationName = '';
    }
  }

  addDesignations() {
    if (!this.activeInputName) {
      this.matSnackbar.open('Please enter a department name', '', {
        duration: 3000
      });
      return;
    }
    this.departmentData.departments[this.activeInputName.index].designations = this.activeInputDesignations;
    this.activeInputName.name = null;
    this.activeInputName.index = null;
    this.activeInputDesignationName = '';
    this.activeInputDesignations = [];
  }

  addSubDepartmentExisting(index, value) {
    if (!value) {
      return;
    }
    this.departmentData.departments[index].designations.push({
      name: value,
      id: randomIdGenerator()
    });
  }

  updateDepartment() {
    this.http.post(this.apiRoutes.postDepartments(), this.departmentData).subscribe(() => {
      this.matSnackbar.open('Successfully updated', '', {
        duration: 3000
      });
    }, (err) => {
      this.matSnackbar.open('Something went wrong', '', {
        duration: 3000
      });
    });
  }

}
