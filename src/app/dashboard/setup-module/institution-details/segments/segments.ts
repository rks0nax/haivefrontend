import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import {InstitutionVerify} from '../../../../services/general';
import {APIRoutes} from '../../../../api.routes';
import {randomIdGenerator} from '../../../../helper-function/random.id.generator';

@Component({
  selector: 'app-institute-details-segment',
  templateUrl: './segments.html',
  styleUrls: ['./segments.scss']
})
export class SegmentsComponent {
  segmentData;
  streams: Array<string> = [];
  edit = [];
  newBoardData = [];
  constructor(private instituteVerify: InstitutionVerify,
              private apiRoutes: APIRoutes,
              private http: HttpClient, private matSnackBar: MatSnackBar) {
    this.segmentData = this.instituteVerify.data.getValue()['segments'];
    if (!this.segmentData) {
      this.segmentData = [];
    }

  }

  addStream(index) {
    if (this.streams[index]) {
      this.segmentData[index]['streams'].push({name: this.streams[index], id: randomIdGenerator()});
      this.streams[index] = '';
    }
  }

  deleteStream(i, j) {
    if (this.segmentData[i].streams.length === 1) {
      this.segmentData.splice(i, 1);
    } else {
      this.segmentData[i]['streams'].splice(j, 1);
    }
  }

  addBoard() {
    if (this.newBoardData.length > 1) {
      this.segmentData.push({board: this.newBoardData[0],
        streams: [{name: this.newBoardData[1], id: randomIdGenerator()
      }]});
      this.newBoardData = [];
    }
  }

  submitData() {
    this.http.post(this.apiRoutes.segmentUpdate(), this.segmentData).subscribe((data) => {
      this.matSnackBar.open('Successfully updated', 'OK', {
        duration: 3000
      });
      this.instituteVerify.refresh();
    }, (err) => {
      this.matSnackBar.open(err.message, 'OK', {
        duration: 3000
      });
    });
  }
}
