import { Component, OnInit } from '@angular/core';
import {InstitutionVerify} from '../../../services/general';
import {APIRoutes} from '../../../api.routes';
import {HttpClient} from '@angular/common/http';
import {FormControl, FormGroup} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
@Component({
  selector: 'app-institution-details',
  templateUrl: './institution-details.component.html',
  styleUrls: ['./institution-details.component.scss']
})
export class InstitutionDetailsComponent implements OnInit {
  instituteFormSubmitted = false;
  instituteForm = new FormGroup({
    'address-street': new FormControl(),
    'address-city': new FormControl(),
    'address-state': new FormControl(),
    'address-zipcode': new FormControl(),
    'inputEmail': new FormControl(),
    'phone': new FormControl(),
    'phoneNumber': new FormControl(),
    'academicyear': new FormControl(),
    'endAcademicYear': new FormControl(),
  });
  instituteName: String;
  instituteSubdomain: String;
  phoneNumber: '';
  phoneCode: '';
  instituteData = {
    'address': {
      'line1': '',
      'line2': '',
      'city': '',
      'state': '',
      'zipCode': '',
      'country': '',
      'phone': []
    },
    'email': '',
    'academicYear': {
      'startYear': '',
      'endYear': ''
    }
  };
  constructor(private instituteVerify: InstitutionVerify,
              private apiRoutes: APIRoutes,
              private http: HttpClient, private matSnackBar: MatSnackBar) {
    this.instituteName = this.instituteVerify.data.getValue()['name'];
    this.instituteData.address = this.instituteVerify.data.getValue()['address'];
    try {
      const splitPhone = this.instituteData.address.phone[0].split('-');
      this.phoneCode = splitPhone[0];
      this.phoneNumber = splitPhone[1];
    } catch (e) {
      console.log('Something wrong with phone number');
    }
    this.instituteData.email = this.instituteVerify.data.getValue()['email'];
    this.instituteData.academicYear = this.instituteVerify.data.getValue()['academicYear'] || {};
    this.instituteSubdomain = location.host.substring(0, location.host.lastIndexOf('.haive.net'));
  }

  ngOnInit() {
  }

  submitData() {

    this.instituteData.address.phone = [this.phoneCode + '-' + this.phoneNumber];
    this.http.post(this.apiRoutes.instituteUpdate(), this.instituteData).subscribe((status) => {
      this.instituteFormSubmitted = true;
      this.matSnackBar.open('Successfully Updated', 'OK', {
        duration: 3000
      });
      this.instituteVerify.refresh();
    }, (err) => {
      console.log(err);
      this.matSnackBar.open(err['error']['message'], 'OK', {
        duration: 3000,
      });
    });
  }
}
