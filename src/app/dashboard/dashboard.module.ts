import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {RouterModule, Routes} from '@angular/router';
import {
  NbLayoutModule, NbSidebarModule, NbSidebarService, NbThemeModule, NbThemeService,
  NbUserModule
} from '@nebular/theme';
import {MatIconModule, MatSelectModule} from '@angular/material';
import {SegmentService} from '../services/segmentServices';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

const DASHBOARD_ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'home',
        loadChildren: './home-module/home-module.module#HomeModule'
      },
      {
        path: 'setup',
        loadChildren: './setup-module/setup.module#SetupModule'
      },
      {
        path: 'academics',
        loadChildren: './academics-module/academics.module#AcademicsModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DASHBOARD_ROUTES),
    FormsModule,
    // Material Modules
    MatIconModule,
    MatSelectModule,
    // Nebular Modules
    NbSidebarModule,
    NbLayoutModule,
    NbThemeModule.forRoot({name: 'default'}),
    NbUserModule,

    // Bootstrap
    NgbModule.forRoot()
  ],
  declarations: [HomeComponent],
  providers: [
    // Nebular Providers
    NbThemeService, NbSidebarService, SegmentService
    // Static Providers
  ]
})
export class DashboardModule { }
