import { Component, OnInit } from '@angular/core';
import {APIRoutes} from '../../../../api.routes';
import {HttpClient} from '@angular/common/http';
import {SegmentService} from '../../../../services/segmentServices';
import {forkJoin} from 'rxjs';
import {randomIdGenerator} from '../../../../helper-function/random.id.generator';

@Component({
  selector: 'app-programs',
  templateUrl: './programs.component.html',
  styleUrls: ['./programs.component.scss']
})
export class ProgramsComponent implements OnInit {

  selectExpanded = false;
  activeInputProgram = '';
  activeInputCompulsorySubjects = [];
  activeInputElective = [];
  programData = {
    segmentId: null,
    programmes: [],
  };
  subjectCategoryData = {
    segmentId: null,
    categories: []
  };
  subjects = {
    segmentId: null,
    subjects: []
  };
  edit = -1;
  constructor(private segmentService: SegmentService, private apiRoutes: APIRoutes, private http: HttpClient) {
    this.segmentService.segmentId.asObservable().subscribe(value => {
      // Reset Data Fields
      this.programData.programmes = [];
      this.subjectCategoryData.categories = [];
      this.subjects.subjects = [];

      const getSubjects = this.http.get(this.apiRoutes.getSubjects(value));
      const getSubjectCategories = this.http.get(this.apiRoutes.getSubjectCategories(value));
      const getProgramData = this.http.get(this.apiRoutes.getProgrammes(value));
      this.programData.segmentId = this.subjectCategoryData.segmentId = this.subjects.segmentId = value;
      // Get all the above api data
      forkJoin([
        getSubjects,
        getSubjectCategories,
        getProgramData,
      ]).subscribe(([subjects, subjectCategories, programData]) => {
          this.subjects = subjects['message'] || this.subjects;
          this.subjectCategoryData = subjectCategories['message'] || this.subjectCategoryData;
          this.programData = programData['message'] || this.programData;
        }, (err) => {
          // Other errors
          console.error('Something went wrong');
          console.error(err);
        });
      });
  }

  ngOnInit() {
  }

  updateProgrammes() {
    if (this.programData.programmes.length === 0) {
      // Error message cant save empty categories
      return;
    }
    this.http.post(this.apiRoutes.postProgrammes(), this.programData).subscribe(response => {
      console.log(response);
    }, (err) => {
      console.error(err);
    });
  }

  /**
   * returns the Objects of elective categories
   * @return {Array<Object<any>>}
   */
  getElectiveCategories() {
    return this.subjectCategoryData.categories.filter((category) => {
      return !category.compulsory;
    });
  }

  getCompulsorySubjects() {
    const compulsoryIndex = this.subjectCategoryData.categories.filter((category) => {
      return category.compulsory;
    });
    return this.subjects.subjects.filter((subject) => {
      return this.subjectCategoryData.categories.find( (category)=> {
        return category.compulsory && subject.category === category.id;
      });
    });
  }

  toggleCompulsory(code) {
    const index = this.activeInputCompulsorySubjects.indexOf(code);
    if (index > -1) {
      this.activeInputCompulsorySubjects.splice(index, 1);
    } else {
      this.activeInputCompulsorySubjects.push(code);
    }
  }

  toggleElective(code) {
    const index = this.activeInputElective.indexOf(code);
    if (index > -1) {
      this.activeInputElective.splice(index, 1);
    } else {
      this.activeInputElective.push(code);
    }
  }

  toggleCompulsoryExisting(code, programIndex) {
    const index = this.programData.programmes[programIndex]['subjects'].indexOf(code);
    if(index > -1) {
      this.programData.programmes[programIndex]['subjects'].splice(index, 1);
    } else {
      this.programData.programmes[programIndex]['subjects'].push(code);
    }
  }

  toggleElectiveExisting(code, programIndex) {
    const index = this.programData.programmes[programIndex]['elective_groups'].indexOf(code);
    if(index > -1) {
      this.programData.programmes[programIndex]['elective_groups'].splice(index, 1);
    } else {
      this.programData.programmes[programIndex]['elective_groups'].push(code);
    }
  }

  addProgramme() {
    if (!this.activeInputProgram || (!this.activeInputCompulsorySubjects.length && !this.activeInputElective.length)) {
      // Error message if name does not exist
      return;
    }
    const data = {
      elective_groups: this.activeInputElective,
      subjects: this.activeInputCompulsorySubjects,
      id: randomIdGenerator(),
      name: this.activeInputProgram
    };
    this.programData.programmes.push(data);
    this.activeInputElective = [];
    this.activeInputCompulsorySubjects = [];
    this.activeInputProgram = '';
    this.selectExpanded = false;
  }

  deleteProgramme(index) {
    this.programData.programmes.splice(index, 1);
  }
}
