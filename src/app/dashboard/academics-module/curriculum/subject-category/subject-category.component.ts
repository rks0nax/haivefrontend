import {Component} from '@angular/core';
import {SegmentService} from '../../../../services/segmentServices';
import {APIRoutes} from '../../../../api.routes';
import {HttpClient} from '@angular/common/http';
import {randomIdGenerator} from '../../../../helper-function/random.id.generator';

@Component({
  selector: 'app-subject-category',
  templateUrl: 'subject-category.component.html',
  styleUrls: ['subject-category.component.scss']
})
export class SubjectCategoryComponent {
  activeInputCategory: string;
  activeInputCompulsory: any = true;
  subjectCategoryData = {
    segmentId: null,
    categories: []
  };
  edit = -1;
  constructor(private segmentService: SegmentService, private apiRoutes: APIRoutes, private http: HttpClient) {
    this.segmentService.segmentId.asObservable().subscribe(value => {
      this.subjectCategoryData.segmentId = value;
      // Reset data
      this.subjectCategoryData.categories = [];
      // Get the subject categories of a segment
      this.http.get(this.apiRoutes.getSubjectCategories(value)).subscribe((data) => {
        this.subjectCategoryData = data['message'] || this.subjectCategoryData;
      }, (err) => {
        // if response is 404 the subject category has been never added before
        // if (err.error.status === 404) {
        //   this.subjectCategoryData.categories = [];
        //   return;
        // }
        // Other errors
        console.error('Something went wrong');
        console.error(err);
      });
    });
  }

  addCategory() {
    if (!this.activeInputCategory) {
      // Display Error, no empty values allowed
      return;
    }
    for (let i = 0; i < this.subjectCategoryData.categories.length; i++) {
      if (this.subjectCategoryData.categories[i].name === this.activeInputCategory) {
        // Display error, no duplicate entries
        return;
      }
    }
    this.subjectCategoryData.categories.push({
      name: this.activeInputCategory,
      compulsory: this.activeInputCompulsory,
      id: randomIdGenerator(),
    });
    this.activeInputCategory = '';
    this.activeInputCompulsory = true;
    console.log(this.subjectCategoryData);
  }
  updateCategories() {
    if (this.subjectCategoryData.categories.length === 0) {
      // Error message cant save empty categories
      return;
    }
    this.http.post(this.apiRoutes.postSubjectCategories(), this.subjectCategoryData).subscribe(response => {
      console.log(response);
    }, (err) => {
      console.error(err);
    });
  }
  deleteCategory(index) {
    this.subjectCategoryData.categories.splice(index, 1);
  }
}
