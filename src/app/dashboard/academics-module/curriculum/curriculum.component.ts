import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-curriculum',
  templateUrl: './curriculum.component.html',
  styleUrls: ['./curriculum.component.scss']
})
export class CurriculumComponent implements OnInit {
  tabs = [
    {
      title: 'Subject Category',
      route: '/v1/dashboard/academics/curriculum/category',
    },
    {
      title: 'Subjects',
      route: '/v1/dashboard/academics/curriculum/subjects',
    },
    {
      title: 'Programs',
      route: '/v1/dashboard/academics/curriculum/programs',
    },
  ];
  constructor(private router: Router) {
    if (location.pathname === '/v1/dashboard/academics/curriculum') {
      router.navigateByUrl(this.tabs[0].route);
    }
  }

  ngOnInit() {
  }

}
