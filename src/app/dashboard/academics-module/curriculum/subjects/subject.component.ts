import {Component} from '@angular/core';
import {SegmentService} from '../../../../services/segmentServices';
import {APIRoutes} from '../../../../api.routes';
import {HttpClient} from '@angular/common/http';
import {randomIdGenerator} from '../../../../helper-function/random.id.generator';
import {forkJoin} from 'rxjs';

@Component({
  selector: 'app-subject-category',
  templateUrl: 'subject.component.html',
  styleUrls: ['subject.component.scss']
})
export class SubjectComponent {
  activeInputName: string;
  activeInputCode: any = '';
  activeInputCategory: any = '';

  subjectCategoryData = {
    segmentId: null,
    categories: []
  };
  subjects = {
    segmentId: null,
    subjects: []
  };
  edit = -1;
  constructor(private segmentService: SegmentService, private apiRoutes: APIRoutes, private http: HttpClient) {
    this.segmentService.segmentId.asObservable().subscribe(value => {
      this.subjectCategoryData.segmentId = this.subjects.segmentId = value;
      // Reset data fields
      this.subjectCategoryData.categories = [];
      this.subjects.subjects = [];

      // Reset add fields
      this.activeInputCategory = '';
      this.activeInputName = '';
      this.activeInputCode = '';
      // Get the subject categories of a segment
      const getSubjects = this.http.get(this.apiRoutes.getSubjects(value));
      const getSubjectCategories = this.http.get(this.apiRoutes.getSubjectCategories(value));
      forkJoin([
        getSubjects,
        getSubjectCategories
      ]).subscribe(([subjects, subjectCategories]) => {
        this.subjectCategoryData = subjectCategories['message'] || this.subjectCategoryData;
        if (this.subjectCategoryData.categories.length) {
          this.activeInputCategory = this.subjectCategoryData.categories[0]['id'];
        }
        this.subjects = subjects['message'] || this.subjects;
      }, (err) => {
        // Other errors
        console.error('Something went wrong');
        console.error(err);
      });
    });
  }

  addSubjects() {
    console.log(this.activeInputCategory);
    if (!this.activeInputCategory) {
      // Display Error, no empty values allowed
      return;
    }
    for (let i = 0; i < this.subjects.subjects.length; i++) {
      if (this.subjects.subjects[i].name === this.activeInputName || this.subjects.subjects[i].code === this.activeInputCode) {
        // Display error, no duplicate entries
        return;
      }
    }
    this.subjects.subjects.push({
      name: this.activeInputName,
      code: this.activeInputCode,
      category: this.activeInputCategory,
    });
    this.activeInputName = '';
    this.activeInputCode = '';
    this.activeInputCategory = this.subjectCategoryData.categories[0]['id'];
  }
  updateSubjects() {
    if (this.subjects.subjects.length === 0) {
      // Error message cant save empty categories
      return;
    }
    this.http.post(this.apiRoutes.postSubjects(), this.subjects).subscribe(response => {
      console.log(response);
    }, (err) => {
      console.error(err);
    });
  }
  deleteSubjects(index) {
    this.subjects.subjects.splice(index, 1);
  }
  getCategoryNameById(id) {
    for (let i = 0; i < this.subjectCategoryData.categories.length; i++) {
      if (this.subjectCategoryData.categories[i]['id'] === id) {
        return this.subjectCategoryData.categories[i]['name'];
      }
    }
  }
}
