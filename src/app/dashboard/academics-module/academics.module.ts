import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {
  MatCheckboxModule,
  MatDatepickerModule, MatIconModule, MatListModule, MatNativeDateModule, MatRadioModule, MatSelectModule,
  MatSnackBarModule
} from '@angular/material';
import {NbLayoutModule, NbRouteTabsetModule, NbSidebarModule, NbTabsetModule, NbThemeModule} from '@nebular/theme';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MenuModule} from '../../custom-modules/menu-module/menu.module';
import {RouterModule, Routes} from '@angular/router';
import { CurriculumComponent } from './curriculum/curriculum.component';
import { TimeTableComponent } from './time-table/time-table.component';
import {SubjectCategoryComponent} from './curriculum/subject-category/subject-category.component';
import {SubjectComponent} from './curriculum/subjects/subject.component';
import { ProgramsComponent } from './curriculum/programs/programs.component';

const DASHBOARD_ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'timetable',
        component: TimeTableComponent
      },
      {
        path: 'curriculum',
        component: CurriculumComponent,
        children: [
          {
            path: 'category',
            component: SubjectCategoryComponent,
          },
          {
            path: 'subjects',
            component: SubjectComponent,
          },
          {
            path: 'programs',
            component: ProgramsComponent,
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DASHBOARD_ROUTES),
    FormsModule,
    ReactiveFormsModule,
    // Material Modules
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatRadioModule,
    MatSelectModule,
    MatCheckboxModule,
    // Nebular Modules
    NbSidebarModule,
    NbLayoutModule,
    NbThemeModule.forRoot({name: 'default'}),
    NbRouteTabsetModule,
    // Other Imports

    // Custom Modules
    MenuModule
  ],
  declarations: [HomeComponent, CurriculumComponent, TimeTableComponent, SubjectCategoryComponent, SubjectComponent, ProgramsComponent]
})
export class AcademicsModule { }
