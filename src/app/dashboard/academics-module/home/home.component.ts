import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  menuItems = [
    {
      title: 'Time Table',
      link: '/v1/dashboard/academics/timetable',
      highlight: '/v1/dashboard/academics/timetable'
    },
    {
      title: 'Test/Examination',
      link: '/v1/dashboard/academics/examination',
      highlight: '/v1/dashboard/academics/examination'
    },
    {
      title: 'Assignment',
      link: '/v1/dashboard/academics/assignment',
      highlight: '/v1/dashboard/academics/assignment'
    },
    {
      title: 'Lesson Planning',
      link: '/v1/dashboard/academics/lesson_planning',
      highlight: '/v1/dashboard/academics/lesson_planning'
    },
    {
      title: 'Curriculum',
      link: '/v1/dashboard/academics/curriculum/',
      highlight: '/v1/dashboard/academics/curriculum/'
    }
  ];
  constructor(private router: Router) {
  }

  ngOnInit() {
  }

}
