import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  menuItems = [
    {
      title: 'Home',
      link: '/v1/dashboard/home',
      highlight: '/v1/dashboard/home'
    },
    {
      title: 'Organisation structure',
      link: '/v1/dashboard/setup/structure',
      highlight: '/v1/dashboard/setup/structure'
    },
    {
      title: 'Users',
      link: '/v1/dashboard/setup/users',
      highlight: '/v1/dashboard/setup/users'
    },
    {
      title: 'Templates',
      link: '/v1/dashboard/templates',
      highlight: '/v1/dashboard/templates'
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
