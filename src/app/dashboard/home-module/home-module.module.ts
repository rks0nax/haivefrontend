import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {RouterModule, Routes} from '@angular/router';
import {NbLayoutModule, NbSidebarModule, NbTabsetModule, NbThemeModule, NbUserModule} from '@nebular/theme';
import {MatDatepickerModule, MatIconModule, MatNativeDateModule, MatSnackBarModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MenuModule} from '../../custom-modules/menu-module/menu.module';

const DASHBOARD_ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: []
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DASHBOARD_ROUTES),
    FormsModule,
    ReactiveFormsModule,
    // Material Modules
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    // Nebular Modules
    NbSidebarModule,
    NbLayoutModule,
    NbThemeModule.forRoot({name: 'default'}),
    NbTabsetModule,
    // Other Imports

    // Custom Modules
    MenuModule
  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
