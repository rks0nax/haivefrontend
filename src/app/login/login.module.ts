import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {APIRoutes} from '../api.routes';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSnackBarModule} from '@angular/material';
import { AdminCreatePasswordComponent } from './admin-create-password/admin-create-password.component';

const LOGIN_ROUTES: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'signup',
    children: [{
      path: 'admin/:hash',
      component: AdminCreatePasswordComponent
    }]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LOGIN_ROUTES),
    FormsModule,
    HttpClientModule,

    // Angular material Imports
    MatSnackBarModule,
  ],
  declarations: [LoginComponent, AdminCreatePasswordComponent],
  providers: [APIRoutes]
})
export class LoginModule { }
