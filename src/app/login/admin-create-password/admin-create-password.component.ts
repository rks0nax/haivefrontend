import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {APIRoutes} from '../../api.routes';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-admin-create-password',
  templateUrl: './admin-create-password.component.html',
  styleUrls: ['./admin-create-password.component.scss']
})
export class AdminCreatePasswordComponent implements OnInit {
  adminHash = '';
  email = '';
  password;
  passwordCopy;
  success = false;
  constructor(private http: HttpClient, private route: ActivatedRoute, private apiEndpoints: APIRoutes,
              private matSnackBar: MatSnackBar, private router: Router) {
    this.route.params.subscribe((params) => {
      this.adminHash = params.hash;
      this.http.get(this.apiEndpoints.getSignupHashDetails(this.adminHash)).subscribe((res) => {
        this.email = res['message'];
      }, (err) => {
        this.matSnackBar.open('Something went wrong', 'OK', {
          duration: 3000
        });
        console.error(err);
      });
    });
  }

  ngOnInit() {

  }

  submit() {
    if (this.password === this.passwordCopy) {
      this.http.post(this.apiEndpoints.setInitialPasswordAdmin(this.adminHash), {password: this.password}).subscribe((res) => {
        this.success = true;
      }, (err) => {
        this.matSnackBar.open('Something went wrong', 'OK', {
          duration: 3000
        });
      });
    } else {
      this.matSnackBar.open('Passwords don\'t match', 'OK', {
        duration: 3000
      });
    }
  }

  goHome() {
    this.router.navigateByUrl('/');
  }
}
