import { Component, OnInit } from '@angular/core';
import {APIRoutes} from '../../api.routes';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userData = {
    password: '',
    email: ''
  };
  constructor(private apiRoutes: APIRoutes, private http: HttpClient, private snackBar: MatSnackBar, private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.http.post(this.apiRoutes.instituteLogin(), this.userData).subscribe((userData) => {
      this.router.navigateByUrl('/v1/dashboard/home');
    }, (err) => {
      const errorMessage = err['error']['message'] || 'Something went wrong';
      this.snackBar.open(errorMessage, 'OK', {
        duration: 5000,
      });
    });
  }
}
