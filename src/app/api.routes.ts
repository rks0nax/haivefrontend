import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';

@Injectable()
export class APIRoutes {
  origin = location.origin + '/api/';
  constructor() {}

  checkInstituteExist(): string {
    return this.origin + 'status';
  }

  instituteLogin(): string {
    return this.origin + 'login';
  }

  getSignupHashDetails(hashId): string {
    return (environment.production ? 'https://eduhive.net/api/' : 'http://localhost:8000/') + 'gethash/' + hashId;
  }

  setInitialPasswordAdmin(hashId): string {
    return (environment.production ? 'https://eduhive.net/api/' : 'http://localhost:8000/') + 'admin_reg/' + hashId;
  }

  instituteUpdate(): string {
    return this.origin + 'setup';
  }

  segmentUpdate(): string {
    return this.origin + 'update_segments';
  }

  getSubjectCategories(segmentId): string {
    return this.origin + 'data/subject_category/' + segmentId;
  }

  postSubjectCategories(): string {
    return this.origin + 'data/subject_category';
  }

  getSubjects(segmentId): string {
    return this.origin + 'data/subjects/' + segmentId;
  }

  postSubjects(): string {
    return this.origin + 'data/subjects';
  }

  getProgrammes(segmentId): string {
    return this.origin + 'data/programmes/' + segmentId;
  }

  postProgrammes(): string {
    return this.origin + 'data/programmes';
  }

  getClasses(segmentId): string {
    return this.origin + 'data/classes/' + segmentId;
  }

  postClasses(): string {
    return this.origin + 'data/classes';
  }

  postHouses(): string {
    return this.origin + 'data/houses';
  }

  getHouses(segmentId): string {
    return this.origin + 'data/houses/' + segmentId;
  }

  postDepartments(): string {
    return this.origin + 'data/departments/';
  }

  getDepartments(segmentId) {
    return this.origin + 'data/departments/' + segmentId;
  }
}
