import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {CookieService} from 'ngx-cookie-service';
import {InstitutionVerify} from './services/general';

// Static services and classes
function isCookiePresent(cookieService: CookieService) {
  return cookieService.check('auth');
}

// End of static services and classes

@Injectable()
export class IsLoggedIn implements CanActivate, CanActivateChild {
  constructor(private cookieService: CookieService, private router: Router) {}
  checkLoggedIn(): Promise<boolean> {
    if (isCookiePresent(this.cookieService)) {
      return Promise.resolve(true);
    }
    this.router.navigateByUrl('/');
    return Promise.resolve(false);
  }
  canActivate(route, state): Observable<boolean>|Promise<boolean> {
    return this.checkLoggedIn();
  }
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkLoggedIn();
  }
}

@Injectable()
export class IsNotLoggedIn implements CanActivate {
  constructor(private cookieService: CookieService, private router: Router) {}
  canActivate(route, state): Promise<boolean> {
    if (!isCookiePresent(this.cookieService)) {
      return Promise.resolve(true);
    }
    this.router.navigateByUrl('/v1/dashboard/home');
    return Promise.resolve(false);
  }
}

@Injectable()
export class InstituteSetupComplete implements CanActivate {
  constructor(private router: Router, private institutionVerify: InstitutionVerify) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise((resolve, reject) => {
      this.institutionVerify.getObserver().subscribe((data) => {
        if (data != null) {
          if (data['config']['progress'] > 6) {
            resolve(true);
          } else {
            this.router.navigateByUrl('/v1/setup');
            resolve(false);
          }
        }
      });
    });
  }
}

@Injectable()
export class InstituteSetupPending implements CanActivate {
  constructor(private router: Router, private institutionVerify: InstitutionVerify) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise((resolve, reject) => {
      this.institutionVerify.getObserver().subscribe((data) => {
        if (data != null) {
          if (data['config']['progress'] < 7) {
            resolve(true);
          } else {
            this.router.navigateByUrl('/v1/dashboard/home');
            resolve(false);
          }
        }
      });
    });
  }

}

@Injectable()
export class InstitutionExist implements CanActivateChild {
  constructor(private institutionVerify: InstitutionVerify, private router: Router) {}
  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise((resolve, reject) => {
      this.institutionVerify.getObserver().subscribe((data) => {
        if (data != null) {
          if (data['verified']) {
            resolve(true);
          } else {
            this.router.navigateByUrl('404');
            resolve(false);
          }
        }
      });
    });
  }
}
