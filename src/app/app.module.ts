import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {
  InstituteSetupComplete, InstituteSetupPending, InstitutionExist, IsLoggedIn,
  IsNotLoggedIn
} from './route.access';
import { NotFoundComponent } from './not-found/not-found.component';
import {APIRoutes} from './api.routes';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {InstitutionVerify} from './services/general';
import {NoopInterceptor} from './services/requestHandler';
import {OverlayModule} from '@angular/cdk/overlay';


const MAIN_ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: './login/login.module#LoginModule',
        canActivate: [IsNotLoggedIn]
      },
      {
        path: 'v1',
        children: [
          {
            path: 'dashboard',
            loadChildren: './dashboard/dashboard.module#DashboardModule',
          }
        ],
        canActivateChild: [IsLoggedIn]
      }
    ],
    canActivateChild: [InstitutionExist]
  },
  {
    path: '404',
    component: NotFoundComponent,
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(MAIN_ROUTES),
    HttpClientModule,
    BrowserAnimationsModule,
    OverlayModule
  ],
  providers: [
    // Route Access
    IsLoggedIn, IsNotLoggedIn, InstitutionExist, InstituteSetupComplete, InstituteSetupPending,
    // Routes
    APIRoutes,
    // Static Classes
    InstitutionVerify,
    // External Services
    CookieService,
    // Other Services
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NoopInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
