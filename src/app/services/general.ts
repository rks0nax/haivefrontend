import {APIRoutes} from '../api.routes';
import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class InstitutionVerify {
  data: BehaviorSubject<Object> = new BehaviorSubject<Object>(null);
  dataLoaded = false;
  constructor(private http: HttpClient, private router: Router, private apiRoutes: APIRoutes) {
    this.refresh().then((data) => {
      // Do something here
    }, (err) => {
      // Something went wrong
    });
  }

  refresh() {
    return new Promise((resolve, reject) => {
      this.dataLoaded = false;
      this.http.get(this.apiRoutes.checkInstituteExist()).subscribe((institutionData) => {
        this.dataLoaded = true;
        this.data.next(institutionData['message']);
        resolve(this.data.getValue());
      }, (err) => {
        this.data.next({verified: false});
        reject(err);
      });
    });
  }

  getObserver() {
    return this.data.asObservable();
  }
}
