import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse
} from '@angular/common/http';

import { Observable } from 'rxjs';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class NoopInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return Observable.create(subscriber => {
      next.handle(req).subscribe(event => {
        if (event instanceof HttpResponse) {
          // console.log(event);
          subscriber.next(event);
          subscriber.complete();
        }
      }, err => {
        subscriber.error(err);
      });
    });
  }
}
