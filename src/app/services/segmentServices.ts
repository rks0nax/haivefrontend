import {Injectable} from '@angular/core';
import {InstitutionVerify} from './general';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class SegmentService {
  segmentData: BehaviorSubject<object> = new BehaviorSubject<object>(null);
  segmentSelected = {
    board: 0,
    stream: 0,
  };
  segmentId: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  constructor(private instituteDetails: InstitutionVerify) {
    this.instituteDetails.getObserver().subscribe((instituteData) => {
      if (instituteData['segments'] && instituteData['segments'].length) {
        this.segmentData.next(instituteData['segments']);
        this.updateSelectedSegment(0, 0);
      }
    });
  }

  updateSelectedSegment(boardIndex, streamIndex) {
    this.segmentSelected.board = boardIndex;
    this.segmentSelected.stream = streamIndex;
    const segmentData = this.segmentData.getValue();
    this.segmentId.next(segmentData[boardIndex]['streams'][streamIndex]['id']);
  }
}
