import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { DrawerOverlayComponent } from './drawer-overlay/drawer-overlay.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DrawerOverlayComponent],
  providers: [],
  exports: [DrawerOverlayComponent]
})
export class DrawerModule { }
