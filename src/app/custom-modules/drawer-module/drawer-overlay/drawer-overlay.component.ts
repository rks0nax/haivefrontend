import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Input} from '@angular/core';

@Component({
  selector: 'app-drawer-overlay',
  templateUrl: './drawer-overlay.component.html',
  styleUrls: ['./drawer-overlay.component.scss']
})
export class DrawerOverlayComponent implements OnInit {

  @Input() show;
  @Output() showChange = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  closeDrawer() {
    this.show = false;
    this.showChange.emit(this.show);
  }

}
