import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrawerOverlayComponent } from './drawer-overlay.component';

describe('DrawerOverlayComponent', () => {
  let component: DrawerOverlayComponent;
  let fixture: ComponentFixture<DrawerOverlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrawerOverlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrawerOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
