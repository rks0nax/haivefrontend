import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'custom-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Input() menuItems: Array<Object>;
  constructor() {
  }
  ngOnInit(): void {}
  menuSelected(url) {
    return location.pathname.indexOf(url) > -1;
  }

}
